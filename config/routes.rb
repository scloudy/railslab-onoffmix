Rails.application.routes.draw do
  resources :projects
  get '/about', to: 'static_pages#about', as: 'about'

  root 'static_pages#about'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
